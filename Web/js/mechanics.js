let table;
$(document).ready(function () {
    table = $("#table").DataTable({
        language: {
            "lengthMenu": "Affiche _MENU_ entrées par page",
            "zeroRecords": "Aucune entrée trouvée",
            "info": "Page _PAGE_ / _PAGES_",
            "search": "Recherche: ",
            "infoEmpty": "Aucune donnée disponible",
            "paginate": {
                "previous": "Précédente",
                "next": "Suivante"
            },
            "infoFiltered": "(filtered from _MAX_ total records)"
        },
        dom: "Bfrtip",
        buttons: [{
            extend: "excelHtml5",
            text: "Exporter en XLSX",
            className: "btn btn-outline-primary",
            autoFilter: true,
            exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            sheetName: "Exported data"
        }],

        data: null,
        columns: [
            {title: "Nom"},
            {title: "Date de naissance"},
            {title: "Date d'ajout"},
            {title: "Couleur favorite"},
            {title: "Boisson favorite"},
            {title: "Salaire"},
            {title: "Semestre"},
            {title: "Hobby"}
        ],
    });
    $.ajax({
        url: "/getAllForms",
        type: "GET",
        success: (data) => {
            data.forEach((entry) => {
                let hobby = entry.Hobby.join(",");
                let date = entry.Date.getDay() + "-" + (entry.Date.getMonth() + 1) + "-" + entry.Date.getFullYear();
                table.row.add([entry.Name, entry.DOB, date, entry.Favourite_Color, entry.Favourite_Drink, entry.Other_Info.Salary, entry.Other_Info.Semester, hobby]).draw();
            });
        },
    });
});

document.querySelector("#resetButton").addEventListener("click", function (event) {
    $("form").get(0).reset();
    event.preventDefault();
}, false);

document.querySelector("#form").addEventListener("submit", function (event) {
    event.preventDefault();
    let formData = $("form").get(0);

    let arrayHobby = String(formData[6].value).split(",");

    let form = {
        Name: formData[0].value,
        DOB: formData[1].value,
        Date: new Date(),
        Favourite_Color: formData[2].value,
        Favourite_Drink: formData[3].value,
        Hobby: arrayHobby,
        Other_Info: {
            Salary: formData[4].value,
            Semester: formData[5].value
        }
    };

    $.ajax({
        url: "/addForm",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(form),
        success: (data) => {
            if (data === "Error") {
                alert("Une erreur s'est produit lors de l'ajout dans la BDD, Verifier les données entrés");
            }
            let date = form.Date.getDay() + "-" + (form.Date.getMonth() + 1) + "-" + form.Date.getFullYear();
            table.row.add([form.Name, form.DOB, date, form.Favourite_Color, form.Favourite_Drink, form.Other_Info.Salary, form.Other_Info.Semester, form.Hobby]).draw();

        },
    });


    $("form").get(0).reset();


}, false);
