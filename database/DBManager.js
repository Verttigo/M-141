const Form = require("./FormModel.js");
const mongoose = require("mongoose");
const app = require("../app");

//MongoOptions
const options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    autoIndex: true,
    keepAlive: true,
    poolSize: 10,
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    family: 4, // Use IPv4, skip trying IPv6
    useFindAndModify: false,
    useUnifiedTopology: true
};

//Initialise the Mongo Connection
mongoose.connect("mongodb://localhost:27017/Forms", options);

//Exports the Mongo Connection
exports.mongoDB = mongoose.connection;

//All Model for the database
exports.FormModel = Form.FormModel;

function isEmpty(obj) {
    for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            return false;
        }
    }
    return JSON.stringify(obj) === JSON.stringify({});
}


/**
 * @desc add Form into the DB
 * @param Array data - the array of data from the client
 */
exports.addForm = function (data, callback) {
    let form = new Form.FormModel(data);
    form.save(function (err) {
        if (err) {
            app.logger.error("Error while adding a new forms : " + form);
            return callback("Error");
        } else {
            app.logger.info("A new form has been submitted");
            return callback(form);
        }
    });

};

/**
 * @desc get all from DB
 * @return Callback- return all forms
 */
exports.getAllForms = function (callback) {
    Form.FormModel.find().then((docs) => {
        if (!(isEmpty(docs))) {
            callback(docs);
        }
    });
};




