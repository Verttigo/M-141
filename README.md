# M-141 : Forms on MongoDB

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/2c7c969ef9bf4830b86b59b262f96025)](https://www.codacy.com/manual/baptiste.ferrando/M-141?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=Verttigo/M-141&amp;utm_campaign=Badge_Grade)

Web APP that let you interact with MongoDB via a form  

---
## Dev Tools

For the dev env you will need Node, Git and the NPM modules installed.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.
   
    $ npm install npm -g

###

## Install

    $ git clone https://gitlab.com/Verttigo/M-141.git
    $ cd M-141
    $ npm install
    

## Running the project

    $ npm start
After you executed this command, Node will create a express web server on port 3000 and connect your MongoDB.