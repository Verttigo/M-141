let mongoose = require("mongoose");


let FormSchema = new mongoose.Schema({
    Name: {
        type: String,
        unique: false
    },
    //date of birth
    DOB: {
        type: String,
        unique: false
    },
    Date: {
        type: String,
        unique: true,
        index: true
    },
    Favourite_Color: {
        type: String,
        unique: false
    },
    Other_Info: {
        Salary: {
            type: String,
            unique: false
        },
        Semester: {
            type: String,
            unique: false
        }
    },
    Favourite_Drink: {
        type: String,
        unique: false
    },
    Hobby: [],
});

let FormModel = mongoose.model("Forms", FormSchema);
exports.FormModel = FormModel;
exports.FormSchema = FormSchema;






