let express = require("express"),
    app = express(),
    db = require("./database/DBManager.js"),
    helmet = require("helmet"),
    bodyParser = require("body-parser"),
    fs = require("fs"),
    {Console} = require("console");


const logger = new Console({stdout: fs.createWriteStream("./app.log"), stderr: fs.createWriteStream("./appError.log")});
exports.logger = logger;

let port = 3000;

//Starting App
app.listen(port, () => {
    db;
    logger.info("App has been started on port : " + 3000);
});


//Expres : Middle Ware params For App
app.use(express.static(__dirname + "/Web/"));
app.use(helmet());
app.use(helmet.noSniff());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


//EndPoint for HTTP Request
app.post("/addForm", function (req, res) {
    db.addForm(req.body, function (callback) {
        res.send(callback);
    });
});

app.get("/getAllForms", function (req, res) {
    db.getAllForms(function (callback) {
        res.send(callback);
    });
});

